const mongoose = require("mongoose");
const dataSchema = new mongoose.Schema({
  VisitDate: {
    type: String,
  },
  visitStatus: {
    type: String,
  },
  Billable: {
    type: String,
  },
  Hrs: {
    type: Number,
  },
  Task: {
    type: String,
  },
  Clinician: {
    type: String,
  },
  Patient: {
    type: String,
  },
  zipCode: {
    type: Number,
  },
  Address: {
    type: String,
  },
});

const Data = mongoose.model("Data", dataSchema);

module.exports = Data;
