const router = require("express").Router();
const registerUser = require("../controller/registerUser");
const resgisterData = require("../controller/registerData");
const userLogin = require("../controller/userLogin");
const getAllUsers = require("../controller/getAllUsers");
const getAllClinicianData = require("../controller/getAllClinicianData");
//register the user

router.post("/api/register", registerUser);

//post for login user is legit or not

router.post("/api/login", userLogin);

//get all user
router.get("/api/getAll", getAllUsers);

//post data

router.post("/api/data", resgisterData);

//get the data route

router.get("/api/data", getAllClinicianData);

module.exports = router;
