const Data = require("../models/data");

const getAllData = async (req, res) => {
  try {
    const allData = await Data.find({});
    res.status(200).json({ status: "success", data: allData });
  } catch (error) {
    res.send(error);
  }
};

module.exports = getAllData;
