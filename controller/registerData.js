const Data = require("../models/data");

const resgisterData = async (req, res) => {
  try {
    const {
      VisitDate,
      visitStatus,
      Billable,
      Hrs,
      Task,
      Clinician,
      Patient,
      zipCode,
      Address,
    } = req.body;

    const data = new Data({
      date: VisitDate,
      status: visitStatus,
      billable: Billable,
      hrs: Hrs,
      task: Task,
      clinician: Clinician,
      patient: Patient,
      zipcode: zipCode,
      address: Address,
    });

    console.log("real data is", data);
    const dataToSave = await Data.save(data);
    // user.push(dataToSave);
    console.log("data is ", dataToSave);
    res.status(200).json({
      status: "Sucess",
      data: {
        data,
      },
    });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = resgisterData;
